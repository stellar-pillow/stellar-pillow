import { AtkStrategy, DefStrategy, Buff, DamagePct } from '.';

export interface ModDb {
  id: number;
  level: number;
}

export interface ShipDb {
  id: number;
  level: number;
  curHealth: number;
  atkStrategy: AtkStrategy;
  defStrategy: DefStrategy;
}

export interface ShuttleDb {
  id: number;
  amount: number;
  atkStrategy: AtkStrategy;
  defStrategy: DefStrategy;
}

export interface BuffDb extends Buff {
  atk: number;
  def: number;
  spd: number;
  damage: DamagePct;
  resist: DamagePct;
}
