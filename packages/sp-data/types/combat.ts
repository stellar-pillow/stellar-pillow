export interface Damage {
  value: number;
  type: DamageType;
}

export const enum DamageType {
  Kinetic,
  Thermal,
  Explosive,
}

// 0 is the default value
export type DamagePct = [number, number, number];

export interface Shield {
  value: number;
  resist: DamagePct;
}

export const enum AtkStrategy {
  Flagship,
  LessHealth,
  MoreAttack,
  LessResist,
}

export const enum DefStrategy {
  Tank,
  Normal,
  Avoid,
}
