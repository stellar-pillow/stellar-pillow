import { Target, Damage, Shield, Buff } from '.';

export interface Mod {
  label: string;
  desc: string;
  size: number;
  modType: ModType;
  target?: Target;
  damage?: (level: number) => Damage;
  shield?: (level: number) => Shield;
  buff?: (level: number) => Buff;
  effect?: (level: number) => number;
}

export const enum ModType {
  Weapon,
  Shield,
  Support,
  All,
}
