import { DamagePct, Damage } from '.';

export interface Cost {
  metal: number;
  crystal: number;
  pop: number;
  ms: number;
}

export const enum Maker {
  Unknown,
  Union,
  Empire,
  Republic,
  State,
  Group,
}

export const enum Target {
  Shuttle,
  Ship,
  Both,
}

export const enum ItemType {
  Mod,
  Ship,
  Shuttle,
}

// usable by js
export enum PlanetThread {
  Idle,
  Upgrade,
  Downgrade,
  Produce,
}

export interface Buff {
  atk?: number;
  def?: number;
  spd?: number;
  damage?: DamagePct;
  resist?: DamagePct;
}

export interface Ship {
  label: string;
  desc: string;
  class: ShipClass;
  maker: Maker;
  modLimit: (level: number) => ModLimit;
  slots: number;
  size: (level: number) => number;
  health: (level: number) => number;
  speed: (level: number) => number;
  resist: () => DamagePct;
  cargo: (level: number) => number;
  opCost: (level: number) => number;
}

export const enum ShipClass {
  Frigate,
  Destroyer,
  Cruiser,
}

export interface ModLimit {
  [index: number]: number;
}

export interface Shuttle {
  label: string;
  desc: string;
  maker: Maker;
  size: number;
  damage: Damage;
  health: number;
  speed: number;
  resist: () => DamagePct;
  cargo: number;
  cost: Cost;
}

export const enum Time {
  M = 60000,
  H = 60 * M,
  D = 24 * H,
}
