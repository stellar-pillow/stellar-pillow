import { Mod, ModType, DamageType, Target } from './types';

export const mods: Mod[] = [];

mods[0] = {
  label: '质量投射器',
  desc: '利用电磁加速发射炮弹',
  modType: ModType.Weapon,
  size: 1,
  damage: level => ({
    value: 90 + level * 10,
    type: DamageType.Kinetic,
  }),
};
mods[100] = {
  label: '阿尔法护盾',
  desc: '保护星舰',
  modType: ModType.Shield,
  size: 1,
  shield: level => ({
    value: 500 + level * 80,
    resist: [0, -1, 0],
  }),
  target: Target.Ship,
};
mods[200] = {
  label: '外挂引擎',
  desc: '速度变快啦',
  modType: ModType.Support,
  size: 1,
  buff: level => ({
    spd: level * 0.1,
  }),
  target: Target.Ship,
};
mods[300] = {
  label: '回收',
  desc: '每战役后恢复星舰装甲',
  modType: ModType.Support,
  size: 1,
  effect: level => 0.06 + level * 0.02,
};
