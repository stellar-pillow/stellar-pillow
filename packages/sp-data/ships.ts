import { Ship, ShipClass, Maker, ModType as Mt } from './types';

export const ships: Ship[] = [];

ships[0] = {
  label: '三无',
  desc: '不知哪来的星舰',
  class: ShipClass.Frigate,
  maker: Maker.Unknown,
  modLimit: () => ({
    [Mt.All]: 2,
  }),
  slots: 4,
  size: level => Math.floor(10 + level * 2.5),
  health: level => Math.floor(600 + level * 15),
  speed: () => 120,
  resist: () => [0.6, 1, -0.2],
  cargo: level => Math.floor(30 + level * 1.4),
  opCost: level => Math.floor(50 + level * 15),
};
ships[1] = {
  label: '海盗',
  desc: '海盗常用的星舰',
  class: ShipClass.Frigate,
  maker: Maker.Unknown,
  modLimit: () => ({
    [Mt.Weapon]: 1,
    [Mt.Shield]: 1,
  }),
  slots: 3,
  size: level => Math.floor(20 + level * 2.5),
  health: level => Math.floor(700 + level * 15),
  speed: () => 180,
  resist: () => [1, 0.5, 0],
  cargo: level => Math.floor(30 + level * 5),
  opCost: level => Math.floor(50 + level * 15),
};

ships[100] = {
  label: '破晓',
  desc: '多功能星舰',
  class: ShipClass.Frigate,
  maker: Maker.Union,
  modLimit: () => ({
    [Mt.Weapon]: 1,
    [Mt.Shield]: 1,
    [Mt.Support]: 1,
  }),
  slots: 5,
  size: level => Math.floor(20 + level * 2.3),
  health: level => Math.floor(1000 + level * 15),
  speed: () => 140,
  resist: () => [0.6, 1, 0],
  cargo: level => Math.floor(50 + level * 2),
  opCost: level => Math.floor(100 + level * 12),
};
