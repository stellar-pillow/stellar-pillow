import { Time } from './types';

const commonList: number[][] = [
  [],
  [0, 50, 10],
  [0, 100, 20],
  [1, 200, 32],
  [2, 500, 48],
  [4, 1000, 64],
  [6, 2000, 80],
  [10, 4000, 100],
  [15, 10000, 130],
  [20, 20000, 180],
  [25, 40000, 250],
];
const popList: number[] = [
  0,
  0,
  1,
  2,
  4,
  6,
  10,
  15,
  20,
  25,
  30,
  40,
  50,
  60,
  70,
  80,
  90,
  100,
  110,
  120,
  135,
  150,
];
const timeList: number[] = [
  0,
  30000,
  2 * Time.M,
  10 * Time.M,
  30 * Time.M,
  1 * Time.H,
  3 * Time.H,
  8 * Time.H,
  12 * Time.H,
  1 * Time.D,
  1.5 * Time.D,
  2 * Time.D,
  2.5 * Time.D,
  3 * Time.D,
  4 * Time.D,
  5 * Time.D,
  6 * Time.D,
  7 * Time.D,
  8 * Time.D,
  9 * Time.D,
  10 * Time.D,
];
const gateList: number[][] = [
  [],
  [10, 8 * Time.H],
  [15, 7 * Time.H],
  [20, 6 * Time.H],
  [30, 5 * Time.H],
  [40, 4 * Time.H],
  [60, 3 * Time.H],
  [80, 2.5 * Time.H],
  [100, 2 * Time.H],
  [150, 1.5 * Time.H],
  [200, 1 * Time.H],
];

const metal = {
  id: 0,
  label: '金属矿场',
  desc: '从星球中提取金属',
  cost: (level: number) => ({
    metal: 10 * 2 ** level,
    crystal: 5 * 2 ** level,
    pop: popList[level],
    ms: timeList[level] / 2,
  }),
  duration: (level: number) => (4 + level) * Time.H,
  incomePerH: (level: number, buff: number) => {
    return 2.5 * level ** 2 + 10 * level * 8 * buff;
  },
  limitBuff: (level: number) => 1000 * level,
};

const crystal = {
  id: 1,
  label: '水晶矿场',
  desc: '从星球中提取水晶',
  cost: (level: number) => ({
    metal: 10 * 2 ** level,
    crystal: 5 * 2 ** level,
    pop: popList[level],
    ms: timeList[level] / 2,
  }),
  duration: (level: number) => (4 + level) * Time.H,
  incomePerH: (level: number, buff: number) => {
    return 2.5 * level ** 2 + 10 * level * 8 * buff;
  },
  limitBuff: (level: number) => 1000 * level,
};

const pop = {
  id: 2,
  label: '庇护所',
  desc: '生产人口并提高人口上限',
  cost: (level: number) => ({
    metal: 15 * 2 ** level,
    crystal: 5 * 2 ** level,
    pop: 0,
    ms: timeList[level] / 2,
  }),
  duration: (level: number) => (4 + level) * Time.H * 2,
  incomePerH: (_: number, buff: number) => 1 * buff,
  limitBuff: (level: number) => 10 * level,
};

const factory = {
  id: 3,
  label: '加工厂',
  desc: '加工材料，船只，组件',
  cost: (level: number) => ({
    metal: commonList[level][1] * 25,
    crystal: commonList[level][1] * 10,
    pop: commonList[level][0] * 2 + 1,
    ms: timeList[level] * 2,
  }),
};

const scanner = {
  id: 4,
  label: '扫描阵列',
  desc: '解锁探险任务',
  cost: (level: number) => ({
    metal: commonList[level][1] * 15,
    crystal: commonList[level][1] * 15,
    pop: commonList[level][0],
    ms: timeList[level] * 2,
  }),
};

const gate = {
  id: 5,
  label: '星门',
  desc: '在星球之间快速移动',
  cost: (level: number) => ({
    metal: commonList[level][1] * 20,
    crystal: commonList[level][1] * 50,
    pop: (commonList[level][0] + 1) * 3,
    ms: timeList[level] * 10,
  }),
  duration: (level: number) => gateList[level][1],
};

const node = {
  id: 6,
  label: '节点',
  desc: '查看和各大势力的关系，购买物品',
  cost: (level: number) => ({
    metal: commonList[level][1] * 20,
    crystal: commonList[level][1] * 50,
    pop: (commonList[level][0] + 1) * 3,
    ms: timeList[level] * 10,
  }),
}

export const structs = {
  metal,
  crystal,
  pop,
  factory,
  scanner,
  gate,
  node,
  [metal.id]: metal,
  [crystal.id]: crystal,
  [pop.id]: pop,
  [factory.id]: factory,
  [scanner.id]: scanner,
  [gate.id]: gate,
  [node.id]: node,
  length: 7,
};
