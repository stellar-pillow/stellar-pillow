import { Shuttle, Maker, DamageType, Time } from './types';

export const shuttles: Shuttle[] = [];

shuttles[100] = {
  label: '小鸡',
  desc: '小巧的战斗机',
  maker: Maker.Union,
  size: 1,
  damage: {
    value: 20,
    type: DamageType.Kinetic,
  },
  health: 100,
  speed: 300,
  resist: () => [0.2, 0.8, 0.2],
  cargo: 0,
  cost: {
    metal: 70,
    crystal: 0,
    pop: 1,
    ms: 30 * Time.M,
  },
};
shuttles[101] = {
  label: '羊驼',
  desc: '通用运输机',
  maker: Maker.Union,
  size: 2,
  damage: {
    value: 2,
    type: DamageType.Kinetic,
  },
  health: 20,
  speed: 100,
  resist: () => [0, 0.5, 0],
  cargo: 25,
  cost: {
    metal: 50,
    crystal: 0,
    pop: 1,
    ms: 30 * Time.M,
  },
};
shuttles[102] = {
  label: '章鱼',
  desc: '可移动激光发射器',
  maker: Maker.Union,
  size: 3,
  damage: {
    value: 50,
    type: DamageType.Thermal,
  },
  health: 80,
  speed: 80,
  resist: () => [0.05, 0.5, 0.2],
  cargo: 0,
  cost: {
    metal: 50,
    crystal: 100,
    pop: 1,
    ms: 1 * Time.H,
  },
};
shuttles[103] = {
  label: '水母',
  desc: '轰炸机',
  maker: Maker.Union,
  size: 2,
  damage: {
    value: 15,
    type: DamageType.Explosive,
  },
  health: 90,
  speed: 140,
  resist: () => [0.4, 0.8, 0],
  cargo: 0,
  cost: {
    metal: 100,
    crystal: 15,
    pop: 1,
    ms: 1 * Time.H,
  },
};
