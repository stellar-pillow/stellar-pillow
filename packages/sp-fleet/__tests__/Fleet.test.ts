import { ships, mods, Target } from 'sp-data';
import { Fleet } from '../Fleet';
import { shipInfo, aircraftInfo, modInfo } from './data';

describe('Fleet', () => {
  const fleet = new Fleet(shipInfo, aircraftInfo, modInfo);
  describe('flagship', () => {
    test('should have correct resistance set', () => {
      expect(fleet.flagship.resist[0]).toBe(ships[0].resist()[0]);
      expect(fleet.flagship.resist[1]).toBe(ships[0].resist()[1]);
      expect(fleet.flagship.resist[2]).toBe(ships[0].resist()[2]);
    });
    test('should have correct speed set', () => {
      // @ts-ignore
      let buff = 1 + mods[200].buff(7).spd;
      expect(fleet.flagship.speed).toBe(ships[0].speed(1) * buff);
    });
    test('should have the effect of alpha shield', () => {
      expect(fleet.shields[Target.Ship][0]).toEqual(
        mods[100].shield ? mods[100].shield(3) : 0,
      );
      expect(fleet.shields[Target.Ship].length).toBe(1);
    });
  });
});
