import { shipInfo, aircraftInfo, modInfo, modInfoWithoutShield } from './data';
import { Fleet } from '../Fleet';
import { getCombatResult } from '../getCombatResult';
import { Combat } from '../Combat';

describe('Combat', () => {
  test('should give the same result as getCombatResult', () => {
    const fleet1A = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleet1B = new Fleet(shipInfo, aircraftInfo, modInfo);

    const fleet2A = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleet2B = new Fleet(shipInfo, aircraftInfo, modInfo);
    const combat = new Combat(fleet2A, fleet2B);
    let result = undefined;
    while (result === undefined) {
      result = combat.nextTurn();
    }
    expect(getCombatResult(fleet1A, fleet1B)).toBe(result);
    expect(fleet1A).toEqual(fleet2A);
  });
  test('should give the same result with both methods', () => {
    const fleet1A = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleet1B = new Fleet(shipInfo, aircraftInfo, modInfo);
    const combat1 = new Combat(fleet1A, fleet1B);
    const fleet2A = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleet2B = new Fleet(shipInfo, aircraftInfo, modInfo);
    const combat2 = new Combat(fleet2A, fleet2B);
    let result1 = undefined;
    while (result1 === undefined) {
      result1 = combat1.nextTurn();
    }
    let result2 = undefined;
    while (result2 === undefined) {
      result2 = combat2.nextTurn();
    }
    expect(fleet1A).toEqual(fleet2A);
  });
  test('fleet with shield should win', () => {
    const fleetA = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleetB = new Fleet(shipInfo, aircraftInfo, modInfoWithoutShield);
    expect(getCombatResult(fleetA, fleetB)).toBe(true);
  });
  test('defensive fleet should win if equal build', () => {
    const fleetA = new Fleet(shipInfo, aircraftInfo, modInfo);
    const fleetB = new Fleet(shipInfo, aircraftInfo, modInfo);
    expect(getCombatResult(fleetA, fleetB)).toBe(false);
  });
});
