import {
  ShipDb,
  AtkStrategy,
  DefStrategy,
  ShuttleDb,
  ModDb,
  ships,
} from 'sp-data';

export const shipInfo: ShipDb = {
  id: 0,
  level: 1,
  curHealth: ships[0].health(1),
  atkStrategy: AtkStrategy.Flagship,
  defStrategy: DefStrategy.Avoid,
};
export const aircraftInfo: ShuttleDb[] = [
  {
    id: 100,
    amount: 20,
    atkStrategy: AtkStrategy.LessResist,
    defStrategy: DefStrategy.Normal,
  },
  {
    id: 102,
    amount: 2,
    atkStrategy: AtkStrategy.MoreAttack,
    defStrategy: DefStrategy.Normal,
  },
  {
    id: 103,
    amount: 1,
    atkStrategy: AtkStrategy.Flagship,
    defStrategy: DefStrategy.Normal,
  },
];
export const modInfo: ModDb[] = [
  {
    id: 0,
    level: 5,
  },
  {
    id: 100,
    level: 3,
  },
  {
    id: 200,
    level: 7,
  },
];

export const modInfoWithoutShield: ModDb[] = [
  {
    id: 0,
    level: 5,
  },
  {
    id: 200,
    level: 7,
  },
];
