import {
  AtkStrategy,
  Target,
  ShipDb,
  ShuttleDb,
  ModDb,
  Damage,
  DamageType,
  mods,
  Shield,
  BuffDb,
} from 'sp-data';
import { FleetUnit } from './FleetUnit';

export class Fleet {
  public units: FleetUnit[] = [];

  // Shields: [shuttle, flagship, all]
  public shields: [Shield[], Shield[], Shield[]] = [[], [], []];

  constructor(shipInfo: ShipDb, shuttleInfos: ShuttleDb[], modInfos: ModDb[]) {
    // Fill units
    this.units.push(new FleetUnit(shipInfo));
    this.units.push(...shuttleInfos.map(info => new FleetUnit(info)));

    // Init array for buffs
    const buffs: BuffDb[] = [
      {
        atk: 0,
        def: 0,
        spd: 0,
        damage: [0, 0, 0],
        resist: [0, 0, 0],
      },
      {
        atk: 0,
        def: 0,
        spd: 0,
        damage: [0, 0, 0],
        resist: [0, 0, 0],
      },
      {
        atk: 0,
        def: 0,
        spd: 0,
        damage: [0, 0, 0],
        resist: [0, 0, 0],
      },
    ];

    /**
     * Compute Modules' buffs
     */
    for (let i = 0; i < modInfos.length; i++) {
      const mod = mods[modInfos[i].id];
      const lv = modInfos[i].level;

      // Add weapon to flagship
      if (mod.damage) this.flagship.damage = mod.damage(lv);
      // Other buffs
      if (mod.target !== undefined) {
        // Add shield
        if (mod.shield) this.shields[mod.target].push(mod.shield(lv));
        // Buff
        if (mod.buff) {
          // Bonus to flagship
          const modBuff = mod.buff(lv);
          const buff = buffs[mod.target];

          if (modBuff.atk) buff.atk += modBuff.atk;
          if (modBuff.def) buff.def += modBuff.def;
          if (modBuff.spd) buff.spd += modBuff.spd;
          if (modBuff.damage) {
            buff.damage[0] += modBuff.damage[0];
            buff.damage[1] += modBuff.damage[1];
            buff.damage[2] += modBuff.damage[2];
          }
          if (modBuff.resist) {
            buff.resist[0] += modBuff.resist[0];
            buff.resist[1] += modBuff.resist[1];
            buff.resist[2] += modBuff.resist[2];
          }
        }
      }
    } // end of for loop

    // apply buffs to the flagship
    this.flagship.applyBuffs(buffs);
    // ... and to shuttles (i = 1 to omit flagship)
    for (let i = 1; i < this.units.length; i++) {
      this.units[i].applyBuffs(buffs);
    }
  } // end of the constructor

  getPriorityList(strategy: AtkStrategy, damageType: DamageType): number[] {
    switch (strategy) {
      case AtkStrategy.LessHealth: {
        const totalHealth: number[][] = [];
        for (let i = 0; i < this.units.length; i++) {
          const elem = this.units[i];
          totalHealth[i] = [i, elem.totalHealth];
        }
        totalHealth.sort((a, b) => a[1] - b[1]);
        return totalHealth.map(x => x[0]);
      }
      case AtkStrategy.MoreAttack: {
        const totalAttack: number[][] = [];
        for (let i = 0; i < this.units.length; i++) {
          const elem = this.units[i];
          totalAttack[i] = [i, elem.damage.value];
        }
        totalAttack.sort((a, b) => b[1] - a[1]);
        return totalAttack.map(x => x[0]);
      }
      case AtkStrategy.LessResist: {
        const resists: number[][] = [];
        for (let i = 0; i < this.units.length; i++) {
          const elem = this.units[i];
          resists[i] = [i, elem.resist[damageType]];
        }
        resists.sort((a, b) => a[1] - b[1]);
        return resists.map(x => x[0]);
      }
      case AtkStrategy.Flagship:
      default: {
        const array: number[] = [];
        for (let i = 0; i < this.units.length; i++) {
          array.push(i);
        }
        return array;
      }
    }
  } // end of getPriorityList

  get flagship(): FleetUnit {
    return this.units[0];
  }

  /**
   * Apply damage to the fleet and return true if the fleet is defeated
   * @returns {boolean} Is defeated
   */
  attackedBy({ damage, speed, atkStrategy }: FleetUnit): boolean {
    // check global shield
    if (this.shielded(Target.Both, damage)) return false;

    const attackOrder = this.getPriorityList(atkStrategy, damage.type);
    // this index of the ship that will be hit
    let unitIndex = -1;

    for (let i = 0; i < attackOrder.length; i++) {
      const index = attackOrder[i];
      if (!this.units[index].escaped(speed)) {
        unitIndex = index;
        break;
      }
    }

    if (unitIndex !== -1) {
      // if something got hit
      if (unitIndex === 0 && this.shielded(Target.Ship, damage)) {
        // if it's the flagship, apply shield first
        return false;
      } else if (unitIndex !== 0 && this.shielded(Target.Shuttle, damage)) {
        // apply shield for other shuttles
        return false;
      }
      // Since shield run out, damage the structure
      this.units[unitIndex].damaged(damage);

      if (this.units[unitIndex].destroyed) {
        // If that thing is destroyed, remove it
        this.units.splice(unitIndex, 1);
        // If it is a flagship, fleet defeated
        if (unitIndex === 0) return true;
      }
    }
    // speed too low, nothing got hit
    return false;
  }

  private shielded(target: Target, damage: Damage): boolean {
    // Make sure there is at least 1 shield
    if (this.shields[target].length > 0) {
      const shield = this.shields[target][0];
      // TODO: bad thing will happen if resist < 0
      const realDmg = Math.ceil(damage.value / shield.resist[damage.type]);

      if (realDmg > shield.value) {
        // remove shield of index 0
        this.shields[target].splice(0, 1);
      } else {
        shield.value -= realDmg;
      }
      return true;
    } else {
      // No more shield
      return false;
    }
  }
}
