import { Fleet } from './Fleet';

/**
 * Mutate fleet A and B, return true if A is the winner
 * @returns {boolean} A is the winner
 */
export function getCombatResult(fleetA: Fleet, fleetB: Fleet): boolean {
  let isAsTurn = fleetA.flagship.speed > fleetB.flagship.speed;
  let attacker: Fleet;
  let defender: Fleet;
  while (true) {
    // Decide the attacker
    if (isAsTurn) {
      attacker = fleetA;
      defender = fleetB;
    } else {
      attacker = fleetB;
      defender = fleetA;
    }

    for (let i = 0; i < attacker.units.length; i++) {
      if (defender.attackedBy(attacker.units[i])) {
        return isAsTurn;
      }
    }
    isAsTurn = !isAsTurn;
  }
}
