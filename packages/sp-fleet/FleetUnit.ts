import {
  Damage,
  AtkStrategy,
  DefStrategy,
  ShipDb,
  ShuttleDb,
  ships,
  shuttles,
  DamagePct,
  BuffDb,
  Target,
  DamageType,
} from 'sp-data';

function isShipDb(info: ShipDb | ShuttleDb): info is ShipDb {
  return (<ShipDb>info).curHealth !== undefined;
}

export class FleetUnit {
  public amount: number;
  public damage: Damage = { value: 0, type: DamageType.Kinetic };
  public health: number;
  public maxHealth: number;
  public speed: number;
  public resist: DamagePct;
  public atkStrategy: AtkStrategy;
  public defStrategy: DefStrategy;
  private isFlagship = false;

  constructor(flagship: ShipDb);
  constructor(aircrafts: ShuttleDb);
  constructor(info: ShipDb | ShuttleDb) {
    if (isShipDb(info)) {
      // Flagship
      this.amount = 1;
      this.maxHealth = ships[info.id].health(info.level);
      this.health = info.curHealth;
      this.speed = ships[info.id].speed(info.level);
      this.resist = ships[info.id].resist();
      this.isFlagship = true;
    } else {
      // Aircrafts
      this.amount = info.amount;
      this.damage = shuttles[info.id].damage;
      this.maxHealth = shuttles[info.id].health;
      this.health = this.maxHealth;
      this.speed = shuttles[info.id].speed;
      this.resist = shuttles[info.id].resist();
    }
    this.atkStrategy = info.atkStrategy;
    this.defStrategy = info.defStrategy;
  }

  get totalHealth(): number {
    return (this.amount - 1) * this.maxHealth + this.health;
  }

  get destroyed(): boolean {
    return this.amount === 0;
  }

  applyBuffs(buffs: BuffDb[]): void {
    const from = this.isFlagship ? Target.Ship : Target.Shuttle;
    // attack
    const shipAtkBuff =
      buffs[from].atk +
      buffs[from].damage[this.damage.type] +
      buffs[Target.Both].atk +
      buffs[Target.Both].damage[this.damage.type];
    if (shipAtkBuff > 0) {
      this.damage.value *= 1 + shipAtkBuff;
    } else {
      this.damage.value /= 1 + shipAtkBuff;
    }

    // speed
    const shipSpdBuff = buffs[from].spd + buffs[Target.Both].spd;
    if (shipSpdBuff > 0) {
      this.speed *= 1 + shipSpdBuff;
    } else {
      this.speed /= 1 + shipSpdBuff;
    }

    // resistance
    for (let i = 0; i < this.resist.length; i++) {
      this.resist[i] +=
        buffs[from].def +
        buffs[from].resist[i] +
        buffs[Target.Both].def +
        buffs[Target.Both].resist[i];
    }
  }

  escaped(atkSpeed: number): boolean {
    return this.defStrategy === DefStrategy.Avoid && this.speed > atkSpeed;
  }

  /**
   * let the unit take the damage and returns the damage taken
   * @returns {number} Real damage taken
   */
  damaged(damage: Damage): number {
    const realDmg: number = Math.ceil(damage.value / this.resist[damage.type]);
    if (realDmg >= this.totalHealth) {
      const output = this.totalHealth;
      this.amount = 0;
      this.health = 0;
      return output;
    }
    if (realDmg < this.health) {
      this.health -= realDmg;
    } else {
      const newTotHp = this.totalHealth - realDmg;
      this.amount = Math.ceil(newTotHp / this.maxHealth);
      this.health = newTotHp % this.maxHealth;
    }
    return realDmg;
  }
}
