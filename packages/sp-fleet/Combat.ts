import { Fleet } from './Fleet';

export class Combat {
  private _isAttackersTurn: boolean;
  public get isAttackersTurn(): boolean {
    return this._isAttackersTurn;
  }

  private _i: number = 0;
  public get unitIndex(): number {
    return this._i;
  }

  constructor(public attacker: Fleet, public defender: Fleet) {
    this._isAttackersTurn = attacker.flagship.speed > defender.flagship.speed;
  }

  /**
   * return true if attacker wins, false if defender wins, otherwise undefined
   */
  nextRound(): boolean | undefined {
    const [atkFleet, defFleet] = this.isAttackersTurn
      ? [this.attacker, this.defender]
      : [this.defender, this.attacker];

    for (; this._i < atkFleet.units.length; this._i++) {
      const unit = atkFleet.units[this._i];

      if (defFleet.attackedBy(unit)) {
        return this._isAttackersTurn;
      }
    }
    this._isAttackersTurn = !this._isAttackersTurn;
    return undefined;
  }

  /**
   * return true if attacker wins, false if defender wins, otherwise undefined
   */
  nextTurn(): boolean | undefined {
    const [atkFleet, defFleet] = this.isAttackersTurn
      ? [this.attacker, this.defender]
      : [this.defender, this.attacker];

    const unit = atkFleet.units[this._i];

    if (defFleet.attackedBy(unit)) {
      return this._isAttackersTurn;
    }

    if (this._i === atkFleet.units.length - 1) {
      this._isAttackersTurn = !this._isAttackersTurn;
      this._i = 0;
    } else {
      this._i++;
    }

    return undefined;
  }
}
