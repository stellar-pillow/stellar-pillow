import crypto from 'crypto';
import { Document, model, Schema } from 'mongoose';
import { IPlayerInfo } from 'sp-proto';
import { Planet } from '.';

const iterations = 10000;

const playerSchema = new Schema({
  auth: {
    email: {
      value: { type: String, unique: true, sparse: true },
      verified: { type: Boolean, default: false },
    },
    qq: {
      value: { type: String, unique: true, sparse: true },
      verified: { type: Boolean, default: false },
    },
    key: {
      salt: Buffer,
      hash: Buffer,
      iterations: Number,
    },
  },
  name: { type: String },
  credit: { type: Number, default: 200 },
  pillow: { type: Number, default: 20 },
  stats: {
    timeCreated: { type: Date, default: Date.now },
    lastLogin: { type: Date, default: Date.now },
    lastIp: String,
  },
});

playerSchema.methods.newKey = function newKey(
  this: Player,
  password: string,
): void {
  const salt = crypto.randomBytes(16);
  const hash = crypto.pbkdf2Sync(password, salt, iterations, 64, 'sha512');
  this.auth.key = { salt, hash, iterations };
};

playerSchema.methods.matchKey = function matchKey(
  this: Player,
  password: string,
): boolean {
  const key = crypto.pbkdf2Sync(
    password,
    this.auth.key.salt,
    this.auth.key.iterations,
    64,
    'sha512',
  );
  return key.equals(this.auth.key.hash);
};

playerSchema.methods.info = async function(this: Player): Promise<IPlayerInfo> {
  const planets = await Planet.find({ owner: this._id });
  return {
    name: this.name,
    credit: this.credit,
    pillow: this.pillow,
    planets: planets.map(x => ({
      name: x.name,
      stars: x.stars,
      isolated: x.isolated,
    })),
  };
};

export interface Player extends Document {
  auth: {
    email: { value?: string; verified: boolean };
    qq: { value?: string; verified: boolean };
    key: { salt: Buffer; hash: Buffer; iterations: number };
  };
  name: string;
  credit: number;
  pillow: number;
  stats: {
    timeCreated: Date;
    lastLogin: Date;
    lastIp: string;
  };
  newKey: (password: string) => void;
  matchKey: (password: string) => boolean;
  info: () => Promise<IPlayerInfo>;
}

export const Player = model<Player>('Player', playerSchema);
