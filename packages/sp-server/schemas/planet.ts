import { Document, model, Schema, Types } from 'mongoose';
import { ItemType } from 'sp-data';
import { resLimits, prodBuffs } from 'sp-shared';
const ObjectId = Schema.Types.ObjectId;

const pathSchema = new Schema(
  {
    hub: Number,
    distance: Number,
  },
  { _id: false },
);

const structSchema = new Schema(
  {
    id: Number,
    level: Number,
    thread: {
      id: Number,
      time: Number,
    },
  },
  { _id: false },
);

const itemSchema = new Schema(
  {
    type: { type: Number, required: true },
    id: { type: Number, required: true },
    level: Number,
    amount: Number,
  },
  { _id: false },
);

const planetSchema = new Schema({
  name: { type: String, default: 'My planet' },
  owner: { type: ObjectId, required: true, ref: 'Player' },
  stars: {
    metal: { type: Number, default: 0 },
    crystal: { type: Number, default: 0 },
    pop: { type: Number, default: 0 },
    size: { type: Number, default: 0 },
  },
  metal: { type: Number, default: 0 },
  crystal: { type: Number, default: 0 },
  pop: { type: Number, default: 5 },
  paths: [pathSchema],
  structs: [structSchema],
  items: [itemSchema],
});

planetSchema.virtual('isolated').get(function(this: Planet): boolean {
  return !this.paths || !this.paths.length;
});

planetSchema.virtual('max').get(resLimits);
planetSchema.virtual('buffs').get(prodBuffs);

planetSchema.virtual('slots').get(function(this: Planet): number {
  return 8 + this.stars.size;
});

interface Resources {
  metal: number;
  crystal: number;
  pop: number;
}

planetSchema.methods.hasResources = function(
  this: Planet,
  { metal, crystal, pop }: Resources,
): boolean {
  return this.metal >= metal && this.crystal >= crystal && this.pop >= pop;
};

planetSchema.methods.rmResources = function(
  this: Planet,
  { metal, crystal, pop }: Resources,
): void {
  this.metal -= metal;
  this.crystal -= crystal;
  this.pop -= pop;
};

export interface Struct {
  id: number;
  level: number;
  thread: {
    id: number;
    time: number;
  };
}

export interface Item {
  type: ItemType;
  id: number;
  level: number;
  amount: number;
}

export interface Planet extends Document {
  name: string;
  owner: Types.ObjectId;
  stars: {
    metal: number;
    crystal: number;
    pop: number;
    size: number;
  };
  metal: number;
  crystal: number;
  pop: number;
  paths: {
    hub: number;
    distance: number;
  }[];
  structs: Struct[];
  items: Item[];
  isolated: boolean;
  slots: number;
  max: [number, number, number];
  buffs: [number, number, number];
  hasResources: (resource: Resources) => boolean;
  rmResources: (resource: Resources) => void;
}

export const Planet = model<Planet>('Planet', planetSchema);
