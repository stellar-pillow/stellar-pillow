import { Document, model, Schema, Model } from 'mongoose';
import { Time } from 'sp-data';

const signupDelay = Time.D;
const loginDelay = 20 * Time.M;

const ipSchema = new Schema({
  ip: { type: String, unique: true },
  signup: {
    count: { type: Number, default: 0 },
    time: { type: Date, default: Date.now },
  },
  badlogin: {
    count: { type: Number, default: 0 },
    time: { type: Date, default: Date.now },
  },
});

ipSchema.statics.findOrCreate = async function(ip: string) {
  let ipRecord = await Ip.findOne({ ip });
  if (!ipRecord) ipRecord = new Ip({ ip });
  return ipRecord;
};

ipSchema.methods.canRegister = function canRegister() {
  if (Date.now() - this.signup.time < signupDelay) {
    return this.signup.count < 2;
  }
  this.signup.count = 0;
  return true;
};

ipSchema.methods.canLogin = function canLogin() {
  if (this.badlogin.count > 4) {
    if (Date.now() - this.badlogin.time < loginDelay) {
      return false;
    }
    this.badlogin.count = 4;
    return true;
  }
  return true;
};

export interface Ip extends Document {
  ip: string;
  signup: {
    count: number;
    time: Date;
  };
  badlogin: {
    count: number;
    time: Date;
  };
  canRegister: () => boolean;
  canLogin: () => boolean;
}

interface IpModel extends Model<Ip> {
  findOrCreate: (ip: string) => Promise<Ip>;
}

export const Ip = model<Ip, IpModel>('Ip', ipSchema);
