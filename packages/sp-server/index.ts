import mongoose from 'mongoose';
import uWS from 'uWebSockets.js';
import { message } from './ws/message';

const port = 8080;

// Connect to mongodb
mongoose.connect('mongodb://localhost/pillow', { useNewUrlParser: true });
mongoose.set('useCreateIndex', true);

uWS
  .App({})
  .ws('/*', {
    compression: 0,
    maxPayloadLength: 16 * 1024 * 1024,
    idleTimeout: 60,
    open: (ws, req) => {
      ws.ip = req.getHeader('x-real-ip');
      console.log(`New connection from ${ws.ip}`);
    },
    message,
    drain: ws => {
      console.log(`WebSocket backpressure: ${ws.getBufferedAmount()}`);
    },
    close: () => {
      console.log('WebSocket closed');
    },
  })
  .any('/*', res => {
    res.close();
  })
  .listen(port, token => {
    if (token) {
      console.log(`Listening to port ${port}`);
    } else {
      console.log(`Failed to listen to port ${port}`);
    }
  });
