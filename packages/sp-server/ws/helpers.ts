import { WebSocket } from 'uWebSockets.js';
import { ResponseMsg } from 'sp-proto';

export const wsError = (ws: WebSocket, error: ResponseMsg.ErrorId): void => {
  ws.send(
    ResponseMsg.encode({ id: ResponseMsg.Id.ErrorRes, error: error }).finish(),
    true,
  );
  ws.close();
};

export const wsReply = (
  ws: WebSocket,
  id: ResponseMsg.Id,
  content: Uint8Array,
): void => {
  ws.send(ResponseMsg.encode({ id, content }).finish(), true);
};
