import { WebSocketBehavior } from 'uWebSockets.js';
import * as pbRoot from 'sp-proto';
import { client } from '../client';


type MsgHandler = WebSocketBehavior['message'];
export const message: MsgHandler = (ws, message, isBinary) => {
  if (isBinary) {
    // ignore pings
    if (message.byteLength === 1) return;
    try {
      const request = pbRoot.RequestMsg.decode(Buffer.from(message));
      const reqStr = pbRoot.RequestMsg.Id[request.id];
      // @ts-ignore: No index signature
      const req = pbRoot[reqStr].decode(request.content);
      client.emit(reqStr, ws, req);
    } catch (e) {
      console.log(`Error decoding request from ${ws.ip}`);
      ws.close();
    }
  } else {
    console.log(`Non binary request from ${ws.ip}`);
    ws.close();
  }
};
