import { WebSocket } from 'uWebSockets.js';
import { PlanetReq, ResponseMsg as ResMsg, PlanetInfo } from 'sp-proto';
import { Planet } from '../../schemas';
import { wsError, wsReply } from '../../ws/helpers';
import { structs, PlanetThread, Time } from 'sp-data';

export const check = async (
  ws: WebSocket,
  req: PlanetReq,
  planet: Planet,
): Promise<void> => {
  // get the struct
  const s = planet.structs[req.structIndex];
  if (!s) return wsError(ws, ResMsg.ErrorId.Validation);

  switch (s.thread.id) {
    case PlanetThread.Upgrade: {
      const { ms } = structs[s.id].cost(s.level + 1);
      if (s.thread.time + ms <= Date.now()) {
        // Increment struct level and clear thread
        s.level += 1;
        s.thread = { id: PlanetThread.Idle, time: 0 };
      } else {
        return wsError(ws, ResMsg.ErrorId.Validation);
      }

      // if metal, crystal or pop => start production
      if (s.id < 3) {
        s.thread = { id: PlanetThread.Produce, time: Date.now() };
      }

      await planet.save();
      // TODO: fromObject is used as a workaround
      const msg = PlanetInfo.fromObject({
        index: req.planetIndex,
        structs: planet.structs,
      });
      return wsReply(ws, ResMsg.Id.PlanetInfo, PlanetInfo.encode(msg).finish());
    }

    case PlanetThread.Downgrade: {
      const { ms } = structs[s.id].cost(s.level - 1);
      if (s.thread.time + ms <= Date.now()) {
        // Decrement struct level and clear thread
        s.level -= 1;
        s.thread = { id: PlanetThread.Idle, time: 0 };
      } else {
        return wsError(ws, ResMsg.ErrorId.Validation);
      }

      if (s.level < 1) {
        // if struct level is 0, remove it
        planet.structs.splice(req.structIndex, 1);
      } else if (s.id < 3) {
        // if metal, crystal or pop => start production
        s.thread = { id: PlanetThread.Produce, time: Date.now() };
      }

      // add res
      const res = structs[s.id].cost(s.level + 1);
      planet.metal += res.metal * 0.8;
      planet.crystal += res.crystal * 0.8;

      await planet.save();
      const msg = PlanetInfo.fromObject({
        index: req.planetIndex,
        metal: planet.metal,
        crystal: planet.crystal,
        structs: planet.structs,
      });
      return wsReply(ws, ResMsg.Id.PlanetInfo, PlanetInfo.encode(msg).finish());
    }

    case PlanetThread.Produce: {
      if (s.id < 3) {
        let prodType: 'metal' | 'crystal' | 'pop';
        if (s.id === structs.metal.id) prodType = 'metal';
        else if (s.id === structs.crystal.id) prodType = 'crystal';
        else if (s.id === structs.pop.id) prodType = 'pop';
        else return;

        const incomePerH: number = structs[prodType].incomePerH(
          s.level,
          planet.buffs[s.id],
        );
        const duration: number = structs[prodType].duration(s.level);

        if (s.thread.time + duration <= Date.now()) {
          // the production is not 100% completed
          planet[prodType] += Math.round((incomePerH * duration) / Time.H);
        } else {
          // max the player can get
          const hours = (Date.now() - s.thread.time) / Time.H;
          planet[prodType] += Math.floor(incomePerH * hours);
        }
      }

      // restart thread
      s.thread.time = Date.now();

      await planet.save();
      const msg = PlanetInfo.fromObject({
        index: req.planetIndex,
        structs: planet.structs,
        metal: planet.metal,
        crystal: planet.crystal,
        pop: planet.pop,
      });
      return wsReply(ws, ResMsg.Id.PlanetInfo, PlanetInfo.encode(msg).finish());
    }

    default:
      break;
  }
};
