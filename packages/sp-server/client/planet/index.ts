import { WebSocket } from 'uWebSockets.js';
import { PlanetReq, ResponseMsg as ResMsg, PlanetInfo } from 'sp-proto';
import { Planet } from '../../schemas';
import { wsError, wsReply } from '../../ws/helpers';
import { structs, PlanetThread } from 'sp-data';
import { check } from './check';

export const planet = async (ws: WebSocket, req: PlanetReq): Promise<void> => {
  const planets = await Planet.find({ owner: ws.playerId });
  const planet = planets[req.planetIndex];
  if (!planet) return wsError(ws, ResMsg.ErrorId.Validation);

  switch (req.op) {
    case PlanetReq.Op.Check:
      check(ws, req, planet);
      break;

    case PlanetReq.Op.Detail: {
      // TODO: temporary workaround
      const planets: Planet[] = await Planet.find({
        owner: ws.playerId,
      }).lean();
      const planet = planets[req.planetIndex];

      return wsReply(
        ws,
        ResMsg.Id.PlanetInfo,
        PlanetInfo.encode({
          index: req.planetIndex,
          name: planet.name,
          metal: planet.metal,
          crystal: planet.crystal,
          pop: planet.pop,
          structs: planet.structs,
          items: planet.items,
        }).finish(),
      );
    }

    case PlanetReq.Op.Rename: {
      if (req.name.length < 1 || req.name.length > 16) {
        return wsError(ws, ResMsg.ErrorId.Validation);
      }
      planet.name = req.name;
      planet.save();

      return wsReply(
        ws,
        ResMsg.Id.PlanetInfo,
        PlanetInfo.encode({
          index: req.planetIndex,
          name: planet.name,
        }).finish(),
      );
    }

    case PlanetReq.Op.Upgrade: {
      if (req.structIndex > planet.structs.length) {
        return wsError(ws, ResMsg.ErrorId.Validation);
      }
      const struct = planet.structs[req.structIndex];

      let structId: number;
      let nextLv: number;
      if (struct) {
        // Upgrading
        if (
          struct.thread &&
          (struct.thread.id === PlanetThread.Upgrade ||
            struct.thread.id === PlanetThread.Downgrade)
        ) {
          // Struct should not already be upgrading
          return wsError(ws, ResMsg.ErrorId.Validation);
        }
        structId = struct.id;
        nextLv = struct.level + 1;
      } else if (
        req.structId < structs.length &&
        planet.structs.length < planet.slots
      ) {
        // New structure
        structId = req.structId;
        nextLv = 1;
      } else {
        // New structure validation failed
        return wsError(ws, ResMsg.ErrorId.Validation);
      }

      const res = structs[structId].cost(nextLv);
      if (!planet.hasResources(res)) {
        return wsError(ws, ResMsg.ErrorId.Validation);
      }

      // Remove those resources
      planet.rmResources(res);

      // create struct if not exist
      if (!struct)
        planet.structs.push({
          id: structId,
          level: 0,
          thread: { id: PlanetThread.Idle, time: 0 },
        });

      planet.structs[req.structIndex].thread = {
        id: PlanetThread.Upgrade,
        time: Date.now(),
      };

      // save and reply
      await planet.save();
      const msg = PlanetInfo.fromObject({
        index: req.planetIndex,
        metal: planet.metal,
        crystal: planet.crystal,
        pop: planet.pop,
        structs: planet.structs,
      });
      return wsReply(ws, ResMsg.Id.PlanetInfo, PlanetInfo.encode(msg).finish());
    }

    case PlanetReq.Op.Downgrade: {
      if (req.structIndex > planet.structs.length) {
        return wsError(ws, ResMsg.ErrorId.Validation);
      }

      planet.structs[req.structIndex].thread = {
        id: PlanetThread.Downgrade,
        time: Date.now(),
      };

      // save and reply
      await planet.save();
      const msg = PlanetInfo.fromObject({
        index: req.planetIndex,
        structs: planet.structs,
      });
      return wsReply(ws, ResMsg.Id.PlanetInfo, PlanetInfo.encode(msg).finish());
    }

    default:
      console.log('[WARN] PlanetReq default case');
      return wsError(ws, ResMsg.ErrorId.Unknown);
  }
};
