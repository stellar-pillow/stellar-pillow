import { WebSocket } from 'uWebSockets.js';
import { AuthReq, PlayerInfo, ResponseMsg as ResMsg } from 'sp-proto';
import { Ip, Player, Planet } from '../schemas';
import { wsError, wsReply } from '../ws/helpers';

async function badloginInc(ipRecord: Ip) {
  ipRecord.badlogin.count += 1;
  ipRecord.badlogin.time = new Date();
  await ipRecord.save();
}

export const auth = async (ws: WebSocket, req: AuthReq): Promise<void> => {
  switch (req.action) {
    case AuthReq.Action.Login: {
      // Check ip
      const ipRecord = await Ip.findOrCreate(ws.ip);
      if (!ipRecord.canLogin()) return wsError(ws, ResMsg.ErrorId.IpWarning);

      // Get player & check password
      const field = req.id.includes('@') ? 'auth.email.value' : 'auth.qq.value';
      const player = await Player.findOne({ [field]: req.id });
      if (!player || !player.matchKey(req.key)) {
        await badloginInc(ipRecord);
        return wsError(ws, ResMsg.ErrorId.BadLogin);
      }

      // Update stats & attach player to ws
      player.stats.lastLogin = new Date();
      player.stats.lastIp = ws.ip;
      ipRecord.badlogin.count = 0;
      await Promise.all([player.save(), ipRecord.save()]);
      ws.playerId = player._id;

      // reply
      const info = PlayerInfo.encode(await player.info()).finish();
      wsReply(ws, ResMsg.Id.PlayerInfo, info);
      return;
    }

    case AuthReq.Action.Register: {
      // Check ip
      const ipRecord = await Ip.findOrCreate(ws.ip);
      if (!ipRecord.canRegister()) return wsError(ws, ResMsg.ErrorId.IpWarning);

      // Create player
      const field = req.id.includes('@') ? 'auth.email.value' : 'auth.qq.value';
      let player: Player;
      try {
        player = await Player.create({ [field]: req.id });
        player.newKey(req.key);
        player.stats.lastIp = ws.ip;
      } catch (e) {
        if (e.name === 'MongoError' && e.code === 11000) {
          return wsError(ws, ResMsg.ErrorId.DuplicateAcc);
        }
        console.log('[WARN] AuthReq:Register');
        return wsError(ws, ResMsg.ErrorId.Unknown);
      }

      // Save player & create home planet
      await Promise.all([
        player.save(),
        Planet.create({
          name: 'home',
          owner: player._id,
          stars: {
            metal: 0,
            crystal: 0,
            pop: 1,
            size: 3,
          },
          metal: 800,
          crystal: 500,
          pop: 10,
        }),
      ]);

      // reply
      const info = PlayerInfo.encode(await player.info()).finish();
      wsReply(ws, ResMsg.Id.PlayerInfo, info);
      return;
    }

    case AuthReq.Action.ChangeKey: {
      // Get Player & ip
      const player = await Player.findById(ws.playerId);
      const ipRecord = await Ip.findOrCreate(ws.ip);

      // Check old key
      if (!player || !player.matchKey(req.oldKey)) {
        await badloginInc(ipRecord);
        return wsError(ws, ResMsg.ErrorId.BadLogin);
      }

      // Update key
      player.newKey(req.key);
      await player.save();
      return wsError(ws, ResMsg.ErrorId.Success);
    }

    default:
      console.log('[WARN] AuthReq default case');
      return wsError(ws, ResMsg.ErrorId.Unknown);
  }
};
