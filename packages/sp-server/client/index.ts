import { EventEmitter } from 'events';
import { auth } from './auth';
import { planet } from './planet';

export const client = new EventEmitter();

client.on('AuthReq', auth);
client.on('PlanetReq', planet);
