import { structs } from 'sp-data';
import { Planet } from 'sp-server/schemas';

export const resLimits = function(this: Planet): [number, number, number] {
  const max: [number, number, number] = [0, 0, 0];
  for (let i = 0; i < this.structs.length; i++) {
    const s = this.structs[i];
    if (s.id < 3) {
      max[s.id] += (structs[s.id] as any).limitBuff(s.level);
    }
  }
  return max;
};

export const prodBuffs = function(this: Planet): [number, number, number] {
  return [
    1 + this.stars.metal * 0.25,
    1 + this.stars.crystal * 0.25,
    1 + this.stars.pop * 0.25,
  ];
};
